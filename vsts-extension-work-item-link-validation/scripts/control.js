/**
 * The "application" used to implement the delivery plan picker
 *
 */


 var app = (function() {

    function validateWorkItemLinks(self, nextFunc) {
        
        self._tfsWorkItemFormService
            .getFields()
            .then(
                function(fields) {
                
                    let statesThatRequireValidation = self._configuration.workItemStates.trim().split(',');
                    let linkTypesRequired = self._configuration.linkTypes.trim().split(',');

                    let stateField = fields.filter(function(f) { return f.referenceName === 'System.State' });
                    let parentField = fields.filter(function(f) { return f.referenceName === 'System.Parent' });
                    let relatedLinkField = fields.filter(function(f) { return f.referenceName === 'System.RelatedLinkCount' });
                    
                    if( true
                        && stateField.length > 0 
                        && parentField.length > 0 
                        && relatedLinkField.length > 0) {

                        self._tfsWorkItemFormService
                            .getFieldValues([ 'System.State', 'System.Parent', 'System.RelatedLinkCount' ])
                            .then(function(value) {
                                
                                let stateValue = value['System.State'];
                                let parentValue = value['System.Parent'];
                                let relatedLinkCountValue = parseInt(value['System.RelatedLinkCount']);
                                let messageElem = $('#' + self._configuration.errorMessageId);

                                let requireParent = linkTypesRequired.filter(function(l) { return l.trim().toUpperCase() === 'PARENT'; }).length > 0;
                                let requireChild = linkTypesRequired.filter(function(l) { return l.trim().toUpperCase() === 'CHILD'; }).length > 0;
                                let stateRequiresValidation = stateValue !== undefined && stateValue !== null && statesThatRequireValidation.filter(function(s) { return s.trim().toUpperCase() === stateValue.toUpperCase(); }).length > 0;                                                           
                                
                                if(stateRequiresValidation) {
                                    
                                    let allGood = function() {
                                        messageElem.hide();
                                        self._tfsWorkItemFormService.clearError();
                                        VSS.resize(0, 0);
                                    }
    
                                    let notGood = function(errorMessage) {
                                        messageElem.show();
                                        messageElem.text(errorMessage);
                                        self._tfsWorkItemFormService.setError(errorMessage);
                                        VSS.resize(0, 15);
                                    }
    
                                    let noParentLink = function() {
                                        notGood('Parent link required.');
                                    }

                                    let parentLinkCount = (parentValue !== undefined && parentValue !== null) ? 1 : 0;
                                    let childLinkCount = parentLinkCount > 0 ? relatedLinkCountValue - 1 : relatedLinkCountValue;
                                    let parentExpr = parentLinkCount + ' === 1';
                                    let childExprMatches = self._configuration.childLinkCount !== undefined && self._configuration.childLinkCount !== null
                                                                ? self._configuration.childLinkCount.trim().match(/^\s*(?<operator>>|>=|<|<=|!=|!==|==|===)\s*(?<number>\d+)\s*$/)
                                                                : null;
                                    let childExpr = childExprMatches !== null
                                                        ? childLinkCount + ' ' + childExprMatches.groups.operator + ' ' + childExprMatches.groups.number
                                                        : childLinkCount + ' === ' + self._configuration.childLinkCount.trim();

                                    let childCount = childExprMatches !== null
                                                        ? childExprMatches.groups.number
                                                        : self._configuration.childLinkCount.trim();

                                    let childOperatorMesg = (function()
                                    {
                                        if(childExprMatches === null) {
                                            return 'equal to';
                                        }
                                        else if(childExprMatches.groups.operator === '>')
                                        {
                                            return 'greater than';
                                        }
                                        else if(childExprMatches.groups.operator === '>=')
                                        {
                                            return 'greater or equal to';
                                        }
                                        else if(childExprMatches.groups.operator === '<')
                                        {
                                            return 'less than';
                                        }
                                        else if(childExprMatches.groups.operator === '<=')
                                        {
                                            return 'less or equal to';
                                        }
                                        else if(childExprMatches.groups.operator === '!=' || childExprMatches.groups.operator === '!==')
                                        {
                                            return 'NOT equal to';
                                        }
                                        else if(childExprMatches.groups.operator === '==' || childExprMatches.groups.operator === '===')
                                        {
                                            return 'equal to';
                                        }
                                        else{
                                            return childExprMatches.groups.operator;
                                        }                                             
                                    })();

                                    // 00
                                    if(requireParent === false && requireChild === false) {
                                        allGood();
                                    }
                                    // 01
                                    else if(requireParent === false && requireChild === true) {

                                        if(eval(childExpr) === true) {
                                            allGood()
                                        }
                                        else {
                                            notGood('Child link count (' + childLinkCount + ') needs to be ' + childOperatorMesg + ' ' + childCount);
                                        }
                                    }
                                    // 10
                                    else if(requireParent === true && requireChild === false) {

                                        if(eval(parentExpr) === true) {
                                            allGood();                                            
                                        }
                                        else {
                                            noParentLink();
                                        }                                        
                                    }
                                    // 11
                                    else if(requireParent === true && requireChild === true) {

                                        let parentCountOk = eval(parentExpr) === true;
                                        let childCountOk = eval(childExpr) === true;

                                        // 11
                                        if(parentCountOk === true && childCountOk === true) {
                                            allGood();
                                        }
                                        // 01
                                        else if(parentCountOk === false && childCountOk === true) {
                                            noParentLink();
                                        }
                                        // 10
                                        else if(parentCountOk === true && childCountOk === false) {
                                            notGood('Child link count (' + childLinkCount + ') needs to be ' + childOperatorMesg + ' ' + childCount);
                                        }
                                        // 00
                                        else if(parentCountOk === false && childCountOk === false) {
                                            notGood('Parent link required, child link count (' + childLinkCount + ') needs to be ' + childOperatorMesg + ' ' + childCount);
                                        }                                        
                                    }
                                }
                                else {
                                    // nothing to do
                                    messageElem.hide();

                                    self._tfsWorkItemFormService.clearError();

                                    VSS.resize(0, 0);
                                }

                                if(nextFunc !== undefined && nextFunc !== null) {
                                    nextFunc();
                                }
                            });
                    }                         
                }
            );
    }

    return {

        _isInitialising: false,
        _isInitialised: false,

        /* Configuration data

            {
                workItemStates:  inputs.WorkItemStates,
                linkTypes:       inputs.LinkTypes,
                childLinkCount:  inputs.ChildLinkCount
            }
        */
        _configuration: null,

        /* Services */
        _tfsCoreService: null,
        _tfsWorkItemService: null,
        _tfsWorkItemFormService: null,
        _tfsWorkItemTrackingService: null,


        init: function(errorMessageId, onInitialised) {

            let self = this;

            if(self._isInitialising === true || self._isInitialised === true) {

                // nothing to do!
                return;
            }

            // make as being in the processes of initialising
            self._isInitialising = true;

            let registerWorkItemFormListeners = function() {

                // Register a listener for the work item group contribution.

                VSS.register(
                    VSS.getContribution().id,
                    function () {
                        return {
                            // Called when the active work item is modified
                            onFieldChanged: function(args) {
                                
                                console.log("onFieldChanged - " + JSON.stringify(args));
                                
                                validateWorkItemLinks(self);
                            },

                            // Called when a new work item is being loaded in the UI
                            onLoaded: function (args) {
                                
                                console.log("onLoaded - " + JSON.stringify(args));

                                if(self._configuration === null) {

                                    // looks like form has been loaded after a previous one was unloaded...

                                    // retrieve work item form service that is coupled to the current work item form
                                    VSS.require(
                                            ["TFS/WorkItemTracking/Services"],
                                            function(Tfs_WorkItemTracking_Services) {

                                                Tfs_WorkItemTracking_Services
                                                    .WorkItemFormService
                                                    .getService()
                                                    .then(
                                                        function (service) {
            
                                                            // get all the user defined configuration parameters/values
                                                            let inputs = VSS.getConfiguration().witInputs;
            
                                                            if(inputs !== undefined && inputs !== null) {
            
                                                                // retain work item form service
                                                                self._tfsWorkItemFormService = service;
            
                                                                // setup self._configuration again with real data
                                                                self._configuration = {
                                                                    workItemStates: inputs.WorkItemStates,
                                                                    linkTypes:      inputs.LinkTypes,
                                                                    childLinkCount: inputs.ChildLinkCount === undefined || inputs.ChildLinkCount === null || inputs.ChildLinkCount.trim().length === 0 ? '> 0' : inputs.ChildLinkCount,
                                                                    errorMessageId: errorMessageId
                                                                }
            
                                                                validateWorkItemLinks(self);
                                                            }
                                                        }
                                                    );  
                                            });
                                }                              
                            },

                            // Called when the active work item is being unloaded in the UI
                            onUnloaded: function (args) {

                                console.log("onUnloaded - " + JSON.stringify(args));

                                // work item unloaded, clear out current configuration and work item form service
                                self._configuration = null
                                self._tfsWorkItemFormService = null;    
                            },

                            // Called after the work item has been saved
                            onSaved: function (args) {
                                //console.log("onSaved - " + JSON.stringify(args));
                            },

                            // Called when the work item is reset to its unmodified state (undo)
                            onReset: function (args) {
                                console.log("onReset - " + JSON.stringify(args));
                            },

                            // Called when the work item has been refreshed from the server
                            onRefreshed: function (args) {
                                console.log("onRefreshed - " + JSON.stringify(args));
                            }
                        }
                    }
                );             
            }

            let initialiseWorkItemTrackingServices = function(Tfs_WorkItemTracking_Services) {

                // retrieve work item form service that is coupled to the current work item form
                Tfs_WorkItemTracking_Services
                    .WorkItemFormService
                    .getService()
                    .then(
                        function (service) {

                            // retain work item form service
                            self._tfsWorkItemFormService = service;

                            // get all the user defined configuration parameters/values
                            let inputs = VSS.getConfiguration().witInputs;

                            if(inputs !== undefined && inputs !== null) {
                                // setup self._configuration again with real data
                                self._configuration = {
                                    workItemStates: inputs.WorkItemStates,
                                    linkTypes:      inputs.LinkTypes,
                                    childLinkCount: inputs.ChildLinkCount === undefined || inputs.ChildLinkCount === null || inputs.ChildLinkCount.trim().length === 0 ? '> 0' : inputs.ChildLinkCount,
                                    errorMessageId: errorMessageId
                                }

                                validateWorkItemLinks(
                                    self,
                                    function() {
                                        registerWorkItemFormListeners();
                                        onInitialised(); 

                                        self._isInitialising === false;
                                        self._isInitialised = true;
                                    }
                                )
                            }
                            else {
                                onInitialised(); 

                                self._isInitialising === false;
                                self._isInitialised = false;
                            }
                        }
                    );
            }

            // required api services
            let required = [
                "VSS/Service",
                "TFS/Core/RestClient",
                "TFS/Work/RestClient",
                "TFS/WorkItemTracking/RestClient",
                "TFS/WorkItemTracking/Services"
            ];

            VSS.require(
                    required,
                    function(VSS_Service,
                             Tfs_Core_RestClient,
                             Tfs_Work_RestClient,
                             Tfs_WorkItemTracking_RestClient,
                             Tfs_WorkItemTracking_Services) {

                        // retain the Core and WorkItem services
                        self._tfsCoreService                = VSS_Service.getCollectionClient(Tfs_Core_RestClient.CoreHttpClient);
                        self._tfsWorkItemService            = VSS_Service.getCollectionClient(Tfs_Work_RestClient.WorkHttpClient);
                        self._tfsWorkItemTrackingService    = VSS_Service.getCollectionClient(Tfs_WorkItemTracking_RestClient.WorkItemTrackingHttpClient);

                        initialiseWorkItemTrackingServices(Tfs_WorkItemTracking_Services);
                    });
        }
    }
})();