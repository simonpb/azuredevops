var app = (function()
{

    return{
        _changedFieldCount: [],
        _changedFields: [],
        
        _tfsCoreService: null,
        _tfsWorkItemService: null,
        _tfsWorkItemTrackingService: null,
        _tfsWorkItemFormService: null,

        init: function(onInitialised) {

            let self = this;

            // required api services
            let required = [
                "VSS/Service",
                "TFS/Core/RestClient",
                "TFS/Work/RestClient",
                "TFS/WorkItemTracking/RestClient",
                "TFS/WorkItemTracking/Services"
            ];

            let initialiseWorkItemTrackingServices = function(Tfs_WorkItemTracking_Services, nextFunc) {

                // retrieve work item form service that is coupled to the current work item form
                Tfs_WorkItemTracking_Services
                    .WorkItemFormService
                    .getService()
                    .then(
                        function (service) {

                            //service.getWorkItemRelations().then(function(r) { console.log(r) });
                            self._tfsWorkItemFormService = service;

                            if(nextFunc !== undefined && nextFunc !== null) {
                                nextFunc();
                                onInitialised();
                            }
                        }
                    );
                }

            let registerWorkItemFormListeners = function() {

                VSS.register(VSS.getContribution().id, function (context) {
                    return {    
                        // event handlers, called when the active work item is loaded/unloaded/modified/saved
                        onFieldChanged: function (args) {                        

                            changedFields = self._changedFields;
                            changedFieldCount = self._changedFieldCount;

                            if (!changedFields[args.id]) {

                                changedFields[args.id] = [];
                                changedFieldCount[args.id] = 0;
                            }
                            
                            $.each(args.changedFields, function(key, value) {

                                if (!changedFields[args.id][key]) {
                                    changedFields[args.id][key] = value;
                                    changedFieldCount[args.id]++;
                                }
                            });

                            /*
                            VSS
                                .getServiceContribution('VendorPanel.work-item-link-validation-in-test.work-item-link-validation-control-contribution')
                                .then(function(contribution) {
                                    console.log(contribution);
                                });
                            */
                        },
                        onLoaded: function (args) {
                        },
                        onUnloaded: function (args) {
                        },
                        onSaved: function (args) {
                            changedFieldCount[args.id] = 0;
                            changedFields[args.id] = [];
                        },
                        onReset: function (args) {
                            changedFieldCount[args.id] = 0;
                            changedFields[args.id] = [];
                        },
                        onRefreshed: function (args) {
                            changedFieldCount[args.id] = 0;
                            changedFields[args.id] = [];
                        }
                    };
                });
            }

            VSS.require(
                    required,
                    function(VSS_Service,
                                Tfs_Core_RestClient,
                                Tfs_Work_RestClient,
                                Tfs_WorkItemTracking_RestClient,
                                Tfs_WorkItemTracking_Services) {

                        // retain the Core and WorkItem services
                        self._tfsCoreService                = VSS_Service.getCollectionClient(Tfs_Core_RestClient.CoreHttpClient);
                        self._tfsWorkItemService            = VSS_Service.getCollectionClient(Tfs_Work_RestClient.WorkHttpClient);
                        self._tfsWorkItemTrackingService    = VSS_Service.getCollectionClient(Tfs_WorkItemTracking_RestClient.WorkItemTrackingHttpClient);

                        initialiseWorkItemTrackingServices(Tfs_WorkItemTracking_Services, registerWorkItemFormListeners);
                    });                                        
        }
    }
})();