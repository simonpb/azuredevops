Revsion:
* 21/4/2022 - Initial cut

External:
* https://select2.org/configuration/options-api

Howto:
* https://docs.microsoft.com/en-us/azure/devops/extend/get-started/node?view=azure-devops

Publishing:
* https://marketplace.visualstudio.com/manage/publishers/vendorpanel

Reference docs:
* https://docs.microsoft.com/en-us/azure/devops/extend/develop/manifest?view=azure-devops#scopes
* https://docs.microsoft.com/en-us/javascript/api/azure-devops-extension-api/corerestclient	
* https://docs.microsoft.com/en-us/previous-versions/azure/devops/extend/reference/client/api/tfs/work/restclient/workhttpclient2_2?view=tfs-2017

Required npm packages:
* npm install vss-web-extension-sdk --save
* npm i -g tfx-cli

Command to build and package extension:
* npx tfx-cli extension create