/**
 * The "application" used to implement the delivery plan picker
 *
 */


 var app = (function() {

    // private function to build and initialise the drop down state
    function initDropdown (self) {

        let containerId = self._configuration.containerId;
        let projects = self._configuration.allProjects;

        console.log('project name filter: ' + self._configuration.projectNameFilterExpr);
        console.log('plan name filter: ' + self._configuration.planNameFilterExpr);
        console.log('plan desc filter: ' + self._configuration.planDescFilterExpr);
        console.log('data field name: ' + self._configuration.dataFieldName);

        if( true
            && containerId !== undefined
            && containerId !== null
            && projects !== undefined
            && projects !== null) {

            let projNameExpr = new RegExp(self._configuration.projectNameFilterExpr);
            let planNameExpr = new RegExp(self._configuration.planNameFilterExpr);
            let planDescExpr = new RegExp(self._configuration.planDescFilterExpr);
            
            let selectableProjectPlans = new Map();
            let selectablePlans = new Map();

            let fillSelectablePlansWithThoseFilteredIn = function(project, plan) {

                if( true
                    && planNameExpr.test(plan.name) === true
                    && planDescExpr.test(plan.description) === true) {

                    // map of ALL selecteable plans
                    selectablePlans.set(plan.id, plan);

                    // fill map of project -> plan(s)
                    if(selectableProjectPlans.has(project.id) === false) {
                        // new map entry with array of this plan
                        selectableProjectPlans.set(project.id, { project: project, plans: [ plan ] });                                                
                    }
                    else {

                        // project map entry already exists, push the plan
                        selectableProjectPlans.get(project.id).plans.push(plan);
                    }
                }
                else {

                    if(planNameExpr.test(plan.name) === false) {
                        console.log('Skipping plan with name \"' + plan.name + '\" because it does NOT match the expression \"' + self._configuration.planNameFilterExpr + '\"');
                    }

                    if(planDescExpr.test(plan.description) === false) {
                        console.log('Skipping plan (' + plan.name + ') with description \"' + plan.description + '\" because it does NOT match the expression \"' + self._configuration.planDescFilterExpr + '\"');
                    }
                }                
            }

            // build map of valid project -> plan(s)
            // build map of valid plan(s)
            projects
                .forEach(
                    function(project) {

                        console.log('Trying to load plans for project ' + project.name + '...');

                        if(projNameExpr.test(project.name) === true) {

                            if( true
                                && project.plans !== undefined
                                && project.plans !== null
                                && project.plans.length > 0) {

                                // each plan is an option...
                                project.plans.forEach(
                                    function(plan) {

                                        fillSelectablePlansWithThoseFilteredIn(project, plan);
                                    }
                                )
                            }
                        }
                        else {
                            console.log('Skipping project with name \"' + project.name + '\" because it does NOT match the expression \"' + self._configuration.projectNameFilterExpr + '\"');
                        }
                    }
                );

                          
            let fillDropDownWithSelectablePlans = function() {

                let deliveryPlansContainer = $(containerId);
                let deliveryPlansSelect = $(containerId + ' select');

                // make sure drop down is clean...
                if (deliveryPlansSelect.hasClass("select2-hidden-accessible") === true) {

                    // Select2 has been initialized so destroy it.
                    deliveryPlansSelect.select2('destroy');

                    // remove all existing options
                    deliveryPlansSelect.empty();
                }

                // fill drop down with plans...
                if(selectableProjectPlans.size > 0) {

                    // fill the drop down
                    selectableProjectPlans.forEach(
                        function(projectPlans) {

                            let project = projectPlans.project;
                            let plans = projectPlans.plans;
                            let optionGroup = $('<optgroup label=\'' + project.name + '\'>');

                            plans.forEach(
                                function(plan) {
                                    optionGroup.append($('<option value=\'' + plan.id + '\'>').text(plan.name));
                                }
                            );

                            deliveryPlansSelect.append(optionGroup)
                        }
                    )

                    // local to update the backing store field with the current selection
                    let updateFieldWithSelection = function() {

                        let selections = deliveryPlansSelect.select2('data');
                        let values = selections.map(function(selection) { return selection.id }).join(',');

                        console.log(values);

                        // retain the selection in the backing store
                        self
                            ._tfsWorkItemFormService
                            .setFieldValue(self._configuration.dataFieldName, values)
                            .then(
                                function() {
                                    console.log(values);
                                }
                            );
                    }

                    // handlers to capture select2 events that are trigged when selections change...
                    deliveryPlansSelect
                        .on('select2:select',
                            function (e) {                                
                                updateFieldWithSelection();
                                updatePlanWithRequiredFieldCriteria(self, e.params.data.id);

                                if(self._configuration.createReleaseQuery === true) {
                                    createReleaseQueryWithRequiredFieldCriteria(self, e.params.data.id);
                                }
                            }
                        );

                    deliveryPlansSelect
                        .on('select2:unselect',
                            function (e) {
                                updateFieldWithSelection();
                            }
                        );

                    deliveryPlansSelect
                        .on('select2:clear',
                            function (e) {
                                updateFieldWithSelection();
                            }
                        );

                    deliveryPlansSelect
                        .on('select2:open',
                            function (e) {
                                VSS.resize(0, self._controlHeightDropdownExpanded);
                            }
                        );
                        
                    deliveryPlansSelect
                        .on('select2:close',
                            function (e) {
                                VSS.resize(0, self._controlHeightDropdownCollapsed);
                            }
                        );                        
                }
                else {
                  
                    // no options to show...
                    deliveryPlansSelect.append($('<optgroup label=\'No plans available for selection\'>'));                                      
                }

                // turn the select control into a select2 control with multiselect option
                deliveryPlansSelect
                    .select2({
                        placeholder: 'Click to select plan(s)',
                        multiple: true,
                        allowClear: false
                    }); 
                    
                deliveryPlansContainer.show();  
            }

            let fillSelectablePlansWithCurrentPlan = function(planId) {

                let plan = self._configuration.allPlans.has(planId)
                    ? self._configuration.allPlans.get(planId)
                    : null;

                if(plan !== null && selectablePlans.has(plan.id) === false) {

                    // plan is linked to this work item, BUT it does NOT exist in the selectable plans so we need to add it now
                    let project = plan.project;

                    console.log('Project (' + project.name + ') plan (' + plan.name + ') is ear marked for this work item but it is NOT selectable.  Adding it now!');

                    // plan does NOT exist, add it to the selectable plans collection
                    // map of ALL selecteable plans
                    selectablePlans.set(plan.id, plan);

                    // fill map of project -> plan(s)
                    if(selectableProjectPlans.has(project.id) === false) {
                        // new map entry with array of this plan
                        selectableProjectPlans.set(project.id, { project: project, plans: [ plan ] });                                                
                    }
                    else {

                        // project map entry already exists, push the plan
                        selectableProjectPlans.get(project.id).plans.push(plan);
                    }
                }
            }

            let updateSelectablePlansFromFieldValue = function(value) {

                if(value === undefined || value === null || value === '') {

                    // setup dropdown now
                    fillDropDownWithSelectablePlans();

                    // nothing stored in backing field... make sure nothing is selected
                    $(containerId + ' select').val(null).trigger('change');
                }
                else {
                    // Looks like we've got something!
                    // values stored in field should be comma delimited list of plan id's (guids)
                    let selectedValues = value.split(',');

                    // check to see if all the values are guids (plan id)
                    let pattern = new RegExp('^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$', 'i');
                    let validPlanIds = selectedValues.filter(function (value) { return pattern.test(value) === true });
                    
                    // make sure the plans that are selected ALSO appear in the drop down
                    validPlanIds
                        .forEach(
                            function(planId) {

                                fillSelectablePlansWithCurrentPlan(planId);                                
                            }
                        );

                    // setup dropdown now
                    fillDropDownWithSelectablePlans();

                    // update drop down with valid plan id's only
                    $(containerId + ' select').val(validPlanIds).trigger('change');
                }
            }

            // set the selected options as per the data stored in the field specified by
            // self._configuration.dataFieldName
            self
                ._tfsWorkItemFormService
                .getFieldValue(self._configuration.dataFieldName)
                .then(
                    function(value) {

                        updateSelectablePlansFromFieldValue(value);
                    }
                );
        }
    }

    // private function to create a release query that can be used to find all work items with the specified plan id
    function createReleaseQueryWithRequiredFieldCriteria(self, planId) {

        if(planId === undefined || planId === null || planId === '') {

            return;
        }
             
        if(self._configuration.allPlans.has(planId) === true) {

            let plan = self._configuration.allPlans.get(planId);
            let theProject = plan.project;
            
            self
                ._tfsWorkItemTrackingService
                .searchQueries(theProject.id, planId)
                .then(
                    function(queries) {

                        if(queries.value.length <= 0) {

                            let createReleaseQuery = function(parentFolder, prefix) {

                                let wiql = 'SELECT [System.Id],[System.WorkItemType],[System.Title],[System.AssignedTo],[System.State],[System.Tags],[System.AreaPath],[System.IterationPath] FROM workitemLinks WHERE ([Source].[' + self._configuration.dataFieldName + '] CONTAINS \'' + plan.id + '\') AND ([System.Links.LinkType] = \'System.LinkTypes.Hierarchy-Forward\') AND ([Target].[System.WorkItemType] <> \'\') MODE (Recursive)';
                                let namePrefix = prefix === undefined || prefix === null ? '' : prefix;
                                
                                // drop the version number
                                let planNameNoVersion = plan.name.replace(/\d+(.\d+)*(\s*-\s*)?/, "").trim();
                                let queryName = namePrefix + planNameNoVersion + ' (' + plan.id + ')';                                
                                let newQuery = {
                                    name: queryName,
                                    wiql: wiql
                                }

                                self
                                    ._tfsWorkItemTrackingService
                                    .createQuery(newQuery, theProject.id, parentFolder.path)
                                    .then(function(p) {
                                        // query added
                                        console.log('Created query ' + queryName);
                                    });
                            }

                            let createReleaseFolderThenQuery = function(parentFolder, subFolderName) {

                                let newQuery = {
                                    name: subFolderName,
                                    isFolder: true
                                }
                                self
                                    ._tfsWorkItemTrackingService
                                    .createQuery(newQuery, theProject.id, parentFolder.path)
                                    .then(function(subFolder) {

                                        // query folder added, now add the query
                                        createReleaseQuery(subFolder);
                                    });
                            }

                            // release query does NOT exist, create it

                            self
                                ._tfsWorkItemTrackingService
                                .getQueries(theProject.id, 'none', '1')
                                .then(
                                    function(queries) {

                                        let subFolderName = self._configuration.releaseQuerySubFolder;

                                        // get the public shared queries folder
                                        let sharedQueriesFolder = queries.filter(function(query) { return query.isFolder === true && query.isPublic === true && query.name.toUpperCase() === 'SHARED QUERIES'; });

                                        // try to get the subfolder name Releases
                                        let releasesFolder = (sharedQueriesFolder.length === 1 && sharedQueriesFolder[0].hasChildren === true && subFolderName !== null)
                                                                ? sharedQueriesFolder[0].children.filter(function(child) { return child.name.toUpperCase() === subFolderName.toUpperCase() && child.isFolder === true })
                                                                : [];

                                        // try to get the query name Releases
                                        let releasesQuery = (sharedQueriesFolder.length === 1 && sharedQueriesFolder[0].hasChildren === true && subFolderName !== null)
                                                                ? sharedQueriesFolder[0].children.filter(function(child) { return child.name.toUpperCase() === subFolderName.toUpperCase() && child.isFolder === false })
                                                                : [];                                                                

                                        if(releasesFolder.length > 0) {

                                            // Got a subfolder name 'Releases" create a new query in the Releases sub folder
                                            createReleaseQuery(releasesFolder[0]);
                                        }
                                        else if(releasesQuery.length > 0) {

                                            // Got query named 'Releases' in the 'Shared Queries" folder.  Put new query in the root folder
                                            createReleaseQuery(sharedQueriesFolder[0], 'Release ');
                                        }
                                        else {

                                            if(subFolderName !== null) {
                                                // releases folder does NOT exist, create it and then create the new query in it
                                                createReleaseFolderThenQuery(sharedQueriesFolder[0], subFolderName);
                                            }
                                            else {

                                                // No sub folder configured.  Put new query in the root folder
                                                createReleaseQuery(sharedQueriesFolder[0], 'Release ');
                                            }
                                        }
                                    }
                                );
                        }
                    }
                );
        }
    }

    // private function update the applicable plan and make sure it has the required field criteria set
    // so they ONLY show work items that are ear marked for that plan
    function updatePlanWithRequiredFieldCriteria(self, planId) {

        if(planId === undefined || planId === null || planId === '') {

            return;
        }

        // Looks like we've got something!

        // for the selected plan, make sure that the field criteria is set so that ONLY work items that are ear marked for that plan are displayed...

        if(self._configuration.allPlans.has(planId) === true) {

            let plan = self._configuration.allPlans.get(planId);
            let theProject = plan.project;
            
            // yes it is!  So lets make sure the plan has a field critera for the backing store field,
            // but first I need to get the full plan details from Azure DevOps.
            self
                ._tfsWorkItemService
                .getPlan(theProject.id, plan.id)
                .then(
                    function(thePlan) {

                        // ok, got the full plan details,
                        // make sure the plan has the required field criteria and it's configured correctly...

                        /*
                            field criteria should look like this:

                            {
                                fieldName: "Custom.FixVersion"
                                logicalOperator: "AND"
                                operator: "CONTAINS"
                                value: <Plan id goes here>
                            }
                        */

                        // first get all criteria that match the required field name
                        let planUpdated = false;
                        let criterias = thePlan.properties.criteria !== undefined
                                            ? thePlan
                                                .properties
                                                .criteria
                                                .filter(
                                                    function(criteria) {

                                                        return criteria.fieldName === self._configuration.dataFieldName;
                                                    }
                                                )
                                            : [];

                        // have we got any field criteria for the backing store field?
                        if(criterias.length > 0) {

                            // Yes we do!  Make sure existing criteria are configured correctly...
                            criterias
                                .forEach(
                                    function(criteria) {

                                        if( false
                                            || criteria.logicalOperator !== 'AND'
                                            || criteria.operator !== 'CONTAINS'
                                            || criteria.value !== thePlan.id) {

                                            // bummer field criteria is wrong, so lets fix it up!

                                            console.log('Fixing up existing criteria for field (' + self._configuration.dataFieldName +') in project (' + theProject.name + ') plan (' + thePlan.name + ')');

                                            // criteria needs updating
                                            criteria.logicalOperator = 'AND';
                                            criteria.operator = 'CONTAINS';
                                            criteria.value = thePlan.id;

                                            console.log(criteria);

                                            // make plan as having been updated
                                            planUpdated = true;
                                        }
                                    }
                                );
                        }
                        else {

                            // field criteria does NOT exist, so lets add it!
                            console.log('Adding required criteria for field (' + self._configuration.dataFieldName +') in project (' + theProject.name + ') plan (' + thePlan.name + ')');

                            let criteria = {
                                fieldName: self._configuration.dataFieldName,
                                logicalOperator: 'AND',
                                operator: 'CONTAINS',
                                value: thePlan.id
                            }

                            console.log(criteria);

                            if(thePlan.properties.criteria === undefined) {

                                // criteria array missing, add it with new criteria
                                thePlan.properties['criteria'] = [ criteria ]
                            }
                            else
                            {
                                // criteria arry exists, push new criteria
                                thePlan.properties.criteria.push(criteria);
                            }

                            // make plan as having been updated
                            planUpdated = true;
                        }

                        // has plan been updated?
                        if(planUpdated === true) {

                            // Yes, so push updated plan
                            console.log('Updating project (' + theProject.name + ') plan (' + thePlan.name + ')');

                            let updatedPlan = {
                                description: thePlan.description,
                                name: thePlan.name,
                                properties: thePlan.properties,
                                revision: thePlan.revision,
                                type: thePlan.type
                            }

                            console.log(updatedPlan);

                            self
                                ._tfsWorkItemService
                                .updatePlan(updatedPlan, theProject.id, thePlan.id)
                                .then(function(p)
                                {
                                    // plan updated
                                    console.log(p);
                                });
                        }
                    }
                );            
        }   
    }

    return {

        _controlHeightDropdownCollapsed: 80,
        _controlHeightDropdownExpanded: 280,

        _isInitialising: false,
        _isInitialised: false,

        /* Configuration data

            {
                allProjects:            projects,
                allPlans:               plans,
                containerId:            containerId,
                dataFieldName:          inputs.FieldName,
                createReleaseQuery:     inputs.CreateReleaseQuery.toUpperCase() === 'TRUE',
                releaseQuerySubFolder:  inputs.ReleaseQuerySubFolder    === undefined || inputs.ReleaseQuerySubFolder  === null || inputs.ReleaseQuerySubFolder.trim().length === 0 ? null : inputs.ReleaseQuerySubFolder.trim(),                
                projectNameFilterExpr:  inputs.ProjectNameFilter        === undefined || inputs.ProjectNameFilter      === null || inputs.ProjectNameFilter.trim().length     === 0 ? '.*' : inputs.ProjectNameFilter.trim(),
                planNameFilterExpr:     inputs.PlanNameFilter           === undefined || inputs.PlanNameFilter         === null || inputs.PlanNameFilter.trim().length        === 0 ? '.*' : inputs.PlanNameFilter.trim(),
                planDescFilterExpr:     inputs.PlanDescriptionFilter    === undefined || inputs.PlanDescriptionFilter  === null || inputs.PlanDescriptionFilter.trim().length === 0 ? '.*' : inputs.PlanDescriptionFilter.trim()
            }
        */
        _configuration: null,

        /* Services */
        _tfsCoreService: null,
        _tfsWorkItemService: null,
        _tfsWorkItemFormService: null,
        _tfsWorkItemTrackingService: null,


        init: function(containerId, onInitialised) {

            let self = this;

            if(self._isInitialising === true || self._isInitialised === true) {

                // nothing to do!
                return;
            }

            // make as being in the processes of initialising
            self._isInitialising = true;



            let registerWorkItemFormListeners = function() {

                // Register a listener for the work item group contribution.
                VSS.register(
                    VSS.getContribution().id,
                    function () {
                        return {
                            // Called when the active work item is modified
                            onFieldChanged: function(args) {
                                console.log("onFieldChanged - " + JSON.stringify(args));
                            },

                            // Called when a new work item is being loaded in the UI
                            onLoaded: function (args) {

                                console.log("onLoaded - " + JSON.stringify(args));

                                if(self._configuration === null) {
                                    
                                    // configuration is null, looks like the control was unloaded, so lets reinitialise with the current work item form
                                    VSS.require(
                                        ["TFS/WorkItemTracking/Services"],
                                        function(Tfs_WorkItemTracking_Services) {
                        
                                            initialiseWorkItemTrackingServices(Tfs_WorkItemTracking_Services);
                                        });
                                }
                            },

                            // Called when the active work item is being unloaded in the UI
                            onUnloaded: function (args) {

                                console.log("onUnloaded - " + JSON.stringify(args));

                                // work item unloaded, clear out current configuration and work item form service
                                self._configuration = null
                                self._tfsWorkItemFormService = null;                         
                            },

                            // Called after the work item has been saved
                            onSaved: function (args) {
                                console.log("onSaved - " + JSON.stringify(args));

                            },

                            // Called when the work item is reset to its unmodified state (undo)
                            onReset: function (args) {
                                console.log("onReset - " + JSON.stringify(args));
                            },

                            // Called when the work item has been refreshed from the server
                            onRefreshed: function (args) {
                                console.log("onRefreshed - " + JSON.stringify(args));
                            }
                        }
                    }
                );
            }

            let initialiseAppWithProjects = function(projects) {

                let allProjects = new Map();
                let allPlans = new Map();

                // for each project...
                projects.forEach(
                    function(project) {

                        // get plan for the project
                        self._tfsWorkItemService
                            .getPlans(project.id)
                            .then(
                                function(plans) {

                                    // retain the plans in the project object
                                    project['plans'] = plans;

                                    // retain the project in a map for quick access
                                    allProjects.set(project.id, project);

                                    // retain each plan
                                    plans
                                        .forEach(
                                            function(plan) { 

                                                // keep a reference to the associated project
                                                plan['project'] = project;

                                                // keep the plan in a map for quick access
                                                allPlans.set(plan.id, plan) 
                                            }
                                        );
                                }
                            )
                            .then(
                                function() {
                                    // Have the plans for ALL the projects have been retrieved?
                                    if(self._configuration === null && allProjects.size === projects.length) {

                                        // looks like all the plans for all the projects have been retreived, time to finish initialising the app...

                                        // set self._configuration to something so it's NOT null so that this code does NOT get called again...
                                        self._configuration = {}

                                        // get all the user defined configuration parameters/values
                                        let inputs = VSS.getConfiguration().witInputs;

                                        /*
                                            FieldName: "Custom.FixVersion"
                                            PlanDescriptionFilter: "\\s*[S|s]cheduled\\s*"
                                            PlanNameFilter: ".*"
                                            ProjectNameFilter: ".*"
                                        */

                                        // setup self._configuration again with real data
                                        self._configuration = {
                                            allProjects:            allProjects,
                                            allPlans:               allPlans,
                                            containerId:            containerId,
                                            dataFieldName:          inputs.FieldName,
                                            createReleaseQuery:     inputs.CreateReleaseQuery.toUpperCase() === 'TRUE',
                                            releaseQuerySubFolder:  inputs.ReleaseQuerySubFolder    === undefined || inputs.ReleaseQuerySubFolder  === null || inputs.ReleaseQuerySubFolder.trim().length === 0 ? null : inputs.ReleaseQuerySubFolder.trim(),
                                            projectNameFilterExpr:  inputs.ProjectNameFilter        === undefined || inputs.ProjectNameFilter      === null || inputs.ProjectNameFilter.trim().length     === 0 ? '.*' : inputs.ProjectNameFilter.trim(),
                                            planNameFilterExpr:     inputs.PlanNameFilter           === undefined || inputs.PlanNameFilter         === null || inputs.PlanNameFilter.trim().length        === 0 ? '.*' : inputs.PlanNameFilter.trim(),
                                            planDescFilterExpr:     inputs.PlanDescriptionFilter    === undefined || inputs.PlanDescriptionFilter  === null || inputs.PlanDescriptionFilter.trim().length === 0 ? '.*' : inputs.PlanDescriptionFilter.trim()
                                        }

                                        // build the drop down
                                        initDropdown(self);

                                        // register listeners
                                        registerWorkItemFormListeners();

                                        // make app as having been initialised
                                        self._isInitialised = true;
                                        self._isInitialising = false;

                                        // call onInitialised
                                        if(onInitialised !== undefined && onInitialised !== null) {
                                            onInitialised(allProjects);
                                        }
                                    }
                                }
                            );
                    }
                );                
            }

            let getAllProjectsAndPlans = function() {
               
                // get all projects...
                self._tfsCoreService
                    .getProjects()
                    .then(
                        // process the collection of projects
                        function(projects) {

                            initialiseAppWithProjects(projects);
                        }
                    );
            }

            let initialiseWorkItemTrackingServices = function(Tfs_WorkItemTracking_Services) {

                // retrieve work item form service that is coupled to the current work item form
                Tfs_WorkItemTracking_Services
                    .WorkItemFormService
                    .getService()
                    .then(
                        function (service) {

                            // retain work item form service
                            self._tfsWorkItemFormService = service;

                            getAllProjectsAndPlans();
                        }
                    );
            }

            // required api services
            let required = [
                "VSS/Service",
                "TFS/Core/RestClient",
                "TFS/Work/RestClient",
                "TFS/WorkItemTracking/RestClient",
                "TFS/WorkItemTracking/Services"
            ];

            VSS.require(
                    required,
                    function(VSS_Service,
                             Tfs_Core_RestClient,
                             Tfs_Work_RestClient,
                             Tfs_WorkItemTracking_RestClient,
                             Tfs_WorkItemTracking_Services) {

                        // retain the Core and WorkItem services
                        self._tfsCoreService                = VSS_Service.getCollectionClient(Tfs_Core_RestClient.CoreHttpClient);
                        self._tfsWorkItemService            = VSS_Service.getCollectionClient(Tfs_Work_RestClient.WorkHttpClient);
                        self._tfsWorkItemTrackingService    = VSS_Service.getCollectionClient(Tfs_WorkItemTracking_RestClient.WorkItemTrackingHttpClient);

                        initialiseWorkItemTrackingServices(Tfs_WorkItemTracking_Services);
                    });
        }
    }
})();