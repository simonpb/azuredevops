var releases = (function()
{
    var regexRevision   = /\s*(?<revision>\d+(\.\d+)?(\.\d+)?(\.\d+)?)\s+(?<name>.*)/
    var regexArea       = /(\s*(Area)\s*:\s*(?<area>.*)\s*)/i
    var regexDate       = /(\s*Scheduled\s*(Release\s*)?(Date\s*)?:\s*(?<scheduled>.*)\s*)|(\s*Released\s*:\s*(?<released>.*)\s*)/i
    var regexSquad      = /(\s*(Squad|Team)\s*:\s*(?<squad>.*)\s*)/i
    var regexEnv        = /(\s*(Env|Environment)\s*:\s*(?<environment>.*)\s*)/i
    var regexPO         = /(\s*po\s*:\s*(?<po>.*)\s*)/i
    var regexDesc       = /(\s*(Desc|Description)\s*:\s*(?<description>.*)\s*)/i

    function matchRegEx (parts, regEx) {
        for(let i = 0; i < parts.length; i++) {
            let match = parts[i].match(regEx);

            if(match !== null) {
                return match;
            }
        }
        return null;
    } 

    /*
    Class to encapsulate the concept of a release
    */
    class Release {
        constructor() {
            this.projectId      = null;
            this.planId         = null;
            this.name           = '';
            this.version        = '';
            this.area           = '';
            this.squad          = '';
            this.testEnv        = '';
            this.productOwner   = '';
            this.date           = '';
            this.status         = '';
            this.description    = '';
        }

        get planName () {
            return this.version + ' ' + this.name;
        }

        get planDescription () {
            return  'Area: ' + this.area
                    + ' | ' + (this.status == 'released' ? ('Released: ' + this.date) : ('Scheduled: ' + this.date))
                    + ' | ' + 'Squad: ' + this.squad
                    + ' | ' + 'Env: '   + this.testEnv
                    + ' | ' + 'PO: '    + this.productOwner
                    + ' | ' + 'Desc: '  + this.shortDescription;         
        }   
        
        get shortDescription () {

            // limit description to 100 characters
            return this.description.length > 100 
                    ? this.description.substring(0, 97) + '...'
                    : this.description;
        }
        
        saveToStorage() {

            if(this.planId === null) {
                throw 'Cannot save to storage until release has an associated plan id!';
            }
            else {
                let self = this;

                return new Promise(                
                        function(onSuccess, onError) {

                            try {
                                // save release data
                                VSS
                                    .getService(VSS.ServiceIds.ExtensionData)
                                    .then(
                                        function(dataService) {

                                            var collection = VSS.getWebContext().project.id + ".VendorPanel.ReleaseOverviewHub.Releases";
                                            var releaseData = JSON.parse(JSON.stringify(self));

                                            releaseData['id'] = self.planId;
                                            releaseData['__etag'] = -1;

                                            dataService
                                                .setDocument(collection, releaseData)
                                                .then(
                                                    function(doc) {
                                                        onSuccess(doc);
                                                    },
                                                    onError
                                                );
                                        }
                                    );  
                            }
                            catch(ex) {
                                onError(ex);
                            }
                        }
                );
            }
        }
    }

    return {
        /*
        Return a newly minted release
        */
        createNewRelease: function() {

            return new Release();
        },

        /*
        Return a newly minted release based on the information in the plan object
        */        
        createReleaseFromPlan: function(projectId, plan) {

            // parse the plan name for the revision
            let matchVersion    = plan.name.match(regexRevision)

            // parse the plan description for the area, date, dquad, environment, product owner data.
            let parts           = plan.description !== undefined && plan.description !== null ? plan.description.split('|') : []                                    
            let matchArea       = matchRegEx(parts, regexArea);
            let matchDate       = matchRegEx(parts, regexDate);
            let matchSquad      = matchRegEx(parts, regexSquad);
            let matchEnv        = matchRegEx(parts, regexEnv);
            let matchPO         = matchRegEx(parts, regexPO);
            let matchDesc       = matchRegEx(parts, regexDesc);

            // mint new release
            let release = new Release();

            release.projectId       = projectId;
            release.planId          = plan.id;
            release.name            = (matchVersion   !== null    ? matchVersion.groups['name'].trim()      : plan.name);
            release.version         = (matchVersion   !== null    ? matchVersion.groups['revision'].trim()  : '');
            release.area            = (matchArea      !== null    ? matchArea.groups['area'].trim()         : '');
            release.squad           = (matchSquad     !== null    ? matchSquad.groups['squad'].trim()       : '');
            release.testEnv         = (matchEnv       !== null    ? matchEnv.groups['environment'].trim()   : '');
            release.productOwner    = (matchPO        !== null    ? matchPO.groups['po'].trim()             : '');
            release.description     = (matchDesc      !== null    ? matchDesc.groups['description'].trim()  : '');

            if( true
                && matchDate !== null 
                && matchDate.groups['scheduled'] !== undefined 
                && matchDate.groups['scheduled'] !== null) {

                release.status  = 'scheduled';
                release.date    = matchDate.groups['scheduled'].trim();
            }
            else if(true
                    && matchDate !== null 
                    && matchDate.groups['released'] !== undefined 
                    && matchDate.groups['released'] !== null) {

                release.status  = 'released';
                release.date    = matchDate.groups['released'].trim();
            }
            else {
                release.status  = 'scheduled';
                release.date    = '';
            }


            // done
            return release;
        }, 

        loadReleaseFromStorage(projectId, planId) {

            if(planId === undefined || planId === null) {
                throw 'plan id must be provided!';
            }
            else {

                return new Promise(                
                    function(onSuccess, onError) {

                        try {
                            // try to restore configuration data
                            VSS
                                .getService(VSS.ServiceIds.ExtensionData)
                                .then(
                                    function(dataService) {

                                        var collection = VSS.getWebContext().project.id + ".VendorPanel.ReleaseOverviewHub.Releases";
                                        var docId = planId;
                    
                                        dataService
                                            .getDocument(collection, docId)
                                            .then(
                                                function(releaseData) {  
                                                    
                                                    // mint new release
                                                    let release = new Release();

                                                    Object.assign(release, releaseData);

                                                    onSuccess(release);
                                                },
                                                function() {
                                                    onError();
                                                }
                                            );
                                    }
                                );
                        }
                        catch(ex) {
                            onError(ex);
                        }
                    }   
                );
            }
        },

        deleteReleaseFromStorage(projectId, planId) {

            if(planId === undefined || planId === null) {
                throw 'plan id must be provided!';
            }
            else {
                return new Promise(                
                    function(onSuccess, onError) {

                        try {
                            // try to restore configuration data
                            VSS
                                .getService(VSS.ServiceIds.ExtensionData)
                                .then(
                                    function(dataService) {

                                        var collection = VSS.getWebContext().project.id + ".VendorPanel.ReleaseOverviewHub.Releases";
                                        var docId = planId;
                    
                                        dataService
                                            .deleteDocument(collection, docId)
                                            .then(
                                                function() {
                                                    console.log('SUCCESS - Deleted document ' + collection + '.' + docId + ' from storage');
                                                },
                                                function() {
                                                    onError();
                                                }
                                            );
                                    }
                                );
                        }
                        catch(ex) {
                            onError(ex);
                        }
                    }   
                );                
            }
        }        
    }
})();