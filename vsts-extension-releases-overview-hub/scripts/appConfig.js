

var appConfig = (function() {

    return {
        loadConfigData: function() {
            return new Promise(                
                    function(onSuccess, onError) {

                        try {
                            // try to restore configuration data
                            VSS
                                .getService(VSS.ServiceIds.ExtensionData)
                                .then(
                                    function(dataService) {
                                        var collection = VSS.getWebContext().project.id + ".VendorPanel.ReleaseOverviewHub";
                                        var docId = 'configuration';
                    
                                        dataService
                                            // .getDocument(collection, docId, { scopeType: "User" })
                                            .getDocument(collection, docId)
                                            .then(
                                                function(configData) {                                                      
                                                    onSuccess(configData);
                                                },
                                                function() {
                                                    onSuccess(null);
                                                }
                                            );
                                    }
                                );
                        }
                        catch(ex) {
                            onError(ex);
                        }
                    }   
            );
        },

        saveConfigData: function(configData) {
            return new Promise(                
                    function(onSuccess, onError) {

                        try {
                            // save configuration data
                            VSS
                                .getService(VSS.ServiceIds.ExtensionData)
                                .then(
                                    function(dataService) {
                                        var collection = VSS.getWebContext().project.id + ".VendorPanel.ReleaseOverviewHub";
                                        var docId = 'configuration';

                                        configData['id'] = docId;
                                        configData['__etag'] = -1;
                                        
                                        dataService
                                            // .setDocument(collection, configData, { scopeType: "User" })
                                            .setDocument(collection, configData)
                                            .then(
                                                function(doc) {
                                                    onSuccess(doc);
                                                },
                                                onError
                                            );
                                    }
                                );  
                        }
                        catch(ex) {
                            onError(ex);
                        }
                    }
            );
        },

        // loadConfigDataThen: function(nextFunc) { this.loadConfigData().then(nextFunc); },
        // saveConfigDataThen: function(configData, nextFunc) { this.saveConfigData(configData).then(nextFunc); }
    }
})();