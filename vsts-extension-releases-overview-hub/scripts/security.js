
var security = (function()
{
    return {

        getBearerToken: function() {
            return new Promise(
                function(onSuccess, onError) {
                    VSS
                    .require(
                        ["VSS/Authentication/Services"],
                        function (VSS_Auth_Service) {

                            VSS
                                .getAccessToken()
                                .then(
                                    function(token){
                                        
                                        // Format the auth header
                                        let authHeader = VSS_Auth_Service.authTokenManager.getAuthorizationHeader(token);

                                        onSuccess(authHeader);
                                    },
                                    onError
                                );
                        }
                    );
                }
            );
        },

        getSecurityGroups: function(authHeader) {

            return new Promise(
                function(onSuccess, onError) {

                    // GET https://vssps.dev.azure.com/{organization}/_apis/graph/users?api-version=6.0-preview.1
                    let url = 'https://vssps.dev.azure.com/' + VSS.getWebContext().account.name + '/_apis/graph/groups'
                    + '?api-version=7.1-preview.1';

                    // Add token as an Authorization header to your request
                    $.ajax({
                        type: "GET",
                        url: url,
                        beforeSend: function (xhr){ 
                            xhr.setRequestHeader('Authorization', authHeader);
                        },
                        dataType: 'json',
                        success: function (result, status, xhr) {
                            onSuccess(result.value);
                        },
                        error: onError
                    });                    
                }
            );
        },

        getSecurityNamespaceIdForPlans: function(authHeader) {
            return new Promise(
                function(onSuccess, onError) {

                    // GET https://dev.azure.com/fabrikam/_apis/securitynamespaces?api-version=6.0
                    let url = 'https://dev.azure.com/' + VSS.getWebContext().account.name + '/_apis/securitynamespaces'
                            + '?api-version=6.0';

                    $.ajax({
                        type: "GET",
                        url: url,
                        beforeSend: function (xhr){ 
                            xhr.setRequestHeader('Authorization', authHeader);
                        },
                        dataType: 'json',
                        success: function (result, status, xhr) {   
                            let securityNamespacePlan = result.value.filter((sn) => sn.name === 'Plan');

                            if(securityNamespacePlan.length > 0) {

                                console.log('security namespace id for Plan: ' + securityNamespacePlan[0].namespaceId);
                                onSuccess(securityNamespacePlan[0].namespaceId);
                            }
                            else {
                                onError('No security namespace exists with the name \"Plan\"')
                            }
                        },
                        error: onError
                    });                            
                }
            );
        },

        getSubjectDescriptorsForProductOwners: function(authHeader, productOwners) {
            
            return new Promise(
                function(onSuccess, onError) {
                    let promises = [];

                    productOwners
                        .forEach(
                            (productOwner) => {
                        
                                let promise = new Promise(
                                    function(onSuccess, onError) {

                                        try {
                                            // debugger;

                                            // https://vssps.dev.azure.com/vendorpanel/_apis/identities?api-version=6.0&searchFilter=DisplayName&filterValue=Alston%20Chu
                                            let url = 'https://vssps.dev.azure.com/' + VSS.getWebContext().account.name + '/_apis/identities'
                                                    + '?api-version=6.0'
                                                    + '&searchFilter=DisplayName'
                                                    + '&filterValue=' + productOwner

                                            $.ajax({
                                                type: "GET",
                                                url: url,
                                                beforeSend: function (xhr){ 
                                                    xhr.setRequestHeader('Authorization', authHeader);
                                                },
                                                dataType: 'json',
                                                success: function (result, status, xhr) {   

                                                    if(result.value.length > 0) {

                                                        console.log('Subject Descriptor for ' + productOwner + ': ' + result.value[0].subjectDescriptor);

                                                        onSuccess(result.value[0].subjectDescriptor);
                                                    }
                                                    else {
                                                        onSuccess(null);
                                                    }
                                                },
                                                error: function() {
                                                    onSuccess(null);
                                                }
                                            });
                                        }
                                        catch(ex) {
                                            onSuccess(null);
                                        }                         
                                    }
                                );  
                
                                promises.push(promise);
                            }
                        );    
                    
                    //debugger;

                    Promise
                        .all(promises)
                        .then(
                            (values) => { 
                                onSuccess(values.filter((d) => d !== null));
                            },
                            onError);
                }
            );
        },  

        getSubjectDescriptorsForSquads: function(authHeader, squads) {
            
            return new Promise(
                function(onSuccess, onError) {
                    let promises = [];
        
                    squads
                        .forEach(
                            (squad) => {
                        
                                let promise = new Promise(
                                    function(onSuccess, onError) {
                
                                        try {
                                            // debugger;
                
                                            let squadDisplayname = '[' + VSS.getWebContext().project.name + ']\\' + squad;
                
                                            // https://vssps.dev.azure.com/vendorpanel/_apis/identities?api-version=6.0&searchFilter=DisplayName&filterValue=[VendorPanel%20Website]\Squad%20-%20Puma
                                            let url = 'https://vssps.dev.azure.com/' + VSS.getWebContext().account.name + '/_apis/identities'
                                                    + '?api-version=6.0'
                                                    + '&searchFilter=DisplayName'
                                                    + '&filterValue=' + squadDisplayname;
                
                                            $.ajax({
                                                type: "GET",
                                                url: url,
                                                beforeSend: function (xhr){ 
                                                    xhr.setRequestHeader('Authorization', authHeader);
                                                },
                                                dataType: 'json',
                                                success: function (result, status, xhr) {   
                
                                                    if(result.value.length > 0) {
                
                                                        console.log('Subject Descriptor for ' + squadDisplayname + ': ' + result.value[0].subjectDescriptor);
                
                                                        onSuccess(result.value[0].subjectDescriptor);
                                                    }
                                                    else {
                                                        onSuccess(null);
                                                    }
                                                },
                                                error: function() {
                                                    onSuccess(null);
                                                }
                                            });  
                                        } 
                                        catch(ex) {
                                            console.log(ex);
                                            onSuccess(null);
                                        }                         
                                    }
                                );  
                
                                promises.push(promise);
                            }
                        );    
                    
                    //debugger;

                    Promise
                        .all(promises)
                        .then(
                            (values) => { 
                                onSuccess(values.filter((d) => d !== null));
                            },
                            onError);
                }
            );
        },            

        getDescriptorFromSubjectDescriptor: function(authHeader, subjectDescriptor) {
            return new Promise(
                function(onSuccess, onError) {

                    // GET https://vssps.dev.azure.com/vendorpanel/_apis/identities?api-version=6.0&subjectDescriptors=vssgp.Uy0xLTktMTU1MTM3NDI0NS0xNzg4MDE1ODU0LTY0NTc4OTI1MS0zMTgzNjc3OTExLTQxMTU1MzkyMzQtMS0xMTExNzAyMzM5LTIzNjU4Mjk3Ni0zMDcyMTYyOTE1LTI3Mzk5NDc1MjE
                    let url = 'https://vssps.dev.azure.com/' + VSS.getWebContext().account.name + '/_apis/identities'
                            + '?api-version=6.0'
                            + '&subjectDescriptors=' + subjectDescriptor

                    $.ajax({
                        type: "GET",
                        url: url,
                        beforeSend: function (xhr){ 
                            xhr.setRequestHeader('Authorization', authHeader);
                        },
                        dataType: 'json',
                        success: function (result, status, xhr) {   
                            let identities = result.value.filter((identity) => identity.subjectDescriptor === subjectDescriptor);

                            if(identities.length > 0) {

                                console.log('Descriptor for subject descriptor '+ subjectDescriptor + ': ' + identities[0].descriptor);

                                onSuccess(identities[0].descriptor);
                            }
                            else {
                                onError('No identity with subject descriptor ' + subjectDescriptor);
                            }
                        },
                        error: onError
                    });                            
                }
            );
        }
    }
})();