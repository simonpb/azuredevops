/**
 * The "application" used to implement the delivery plan picker
 *
 */

var app = (function() {

    // Read a page's GET URL variables and return them as an associative array.
    function getUrlVars()
    {
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for(var i = 0; i < hashes.length; i++)
        {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    }

    function restoreTableStateThen(self, nextFunc) {

        let applyTableState = function(releaseId, statusFilter, searchFilter, pageLength, tableOrder) {

            // planid = 'f195d68c-ff2b-4743-9a3e-247d167c900b'

            // set status filter & search text
            let releaseIdOk     = releaseId     !== undefined   && releaseId !== null;
            let statusFilterOk  = statusFilter  !== undefined   && statusFilter !== null;
            let searchFilterOk  = searchFilter  !== undefined   && searchFilter !== null;
            let pageLengthOk    = pageLength    !== undefined   && pageLength   !== null;
            let tableOrderOk    = tableOrder    !== undefined   && tableOrder   !== null;

            if( false
                || (releaseIdOk === true)
                || (statusFilterOk === true && $('#releases_search_and_select select').val() !== statusFilter)
                || (searchFilterOk === true && self._table.search() !== searchFilter)
                || (pageLengthOk === true && self._table.page.len() !== pageLength)
                || (tableOrderOk === true && JSON.stringify(self._table.order()) !== tableOrder)) {                                


                let culledReleases = [];

                // if release id provided, this takes precedence...
                if(releaseIdOk === true) {
                    // update status filter UI, is this NOT applied by the datatable, but applyed by us manually
                    $('#releases_search_and_select select').val('all');            

                    // cull releases by release id
                    culledReleases = filterReleasesById(self._releases, releaseId);  

                    // set search to empty, this filter will be applied by the datatable when it is redrawn 
                    self._table.search('');
                }
                else {
                    
                    if(statusFilterOk === true) {
                        // update status filter UI, is this NOT applied by the datatable, but applyed by us manually
                        $('#releases_search_and_select select').val(statusFilter);
                        
                        // cull releases by status filter (if provided)
                        culledReleases = filterReleasesByStatus(self._releases, statusFilter);                     
                    }
                    else {
                        // update status filter UI, is this NOT applied by the datatable, but applyed by us manually
                        $('#releases_search_and_select select').val('all');

                        // nothing to cull
                        culledReleases = self._releases;
                    }

                    // set search filter, this filter will be applied by the datatable when it is redrawn
                    if(searchFilterOk === true) {
                        self._table.search(searchFilter);
                    }
                }

                // set page length (if provided)
                if(pageLengthOk === true) {
                    self._table.page.len(pageLength);
                }                
                
                // apply table order (if provided)
                if(tableOrderOk === true) {
                    self._table.order(JSON.parse(tableOrder));
                }

                self._table.clear();
                self._table.rows.add(culledReleases);                  
                
                // draw
                self._table.draw(false); 
            }
        }

        // try to restore table state from data storage or from parameters provided in url
        VSS
            .getService(VSS.ServiceIds.ExtensionData)
            .then(function(dataService) {
                var collection = VSS.getWebContext().project.id + ".VendorPanel.ReleaseOverviewHub";
                var docId = 'tablestate';

                dataService
                    .getDocument(collection, docId, { scopeType: "User" })
                    .then(
                        function(doc) {
                        
                            let urlVars = getUrlVars();
                            let releaseId = urlVars.filter(v => v === 'releaseId').length > 0 ? decodeURI(urlVars['releaseId']) : null;
                            let statusFilter = urlVars.filter(v => v === 'statusFilter').length > 0 ? decodeURI(urlVars['statusFilter']) : doc.statusFilter;
                            let searchFilter = urlVars.filter(v => v === 'searchFilter').length > 0 ? decodeURI(urlVars['searchFilter']) : doc.searchFilter;
                            
                            applyTableState(releaseId, statusFilter, searchFilter, doc.pageLength, doc.tableOrder);
                            nextFunc();
                        },
                        function() {

                            let urlVars = getUrlVars();
                            let releaseId = urlVars.filter(v => v === 'releaseId').length > 0 ? decodeURI(urlVars['releaseId']) : null;
                            let statusFilter = urlVars.filter(v => v === 'statusFilter').length > 0 ? decodeURI(urlVars['statusFilter']) : null;
                            let searchFilter = urlVars.filter(v => v === 'searchFilter').length > 0 ? decodeURI(urlVars['searchFilter']) : null;                            

                            applyTableState(releaseId, statusFilter, searchFilter);
                            nextFunc();
                        });
            });
    }

    function saveTableState(self) {

        // Save table state to data storage
        VSS.getService(VSS.ServiceIds.ExtensionData).then(function(dataService) {
            var collection = VSS.getWebContext().project.id + ".VendorPanel.ReleaseOverviewHub";
            var docId = 'tablestate';
            var tableStateData = {
                id: docId,
                statusFilter: $('#releases_search_and_select select').val(),
                searchFilter: self._table.search(),
                pageLength: self._table.page.len(),
                tableOrder: JSON.stringify(self._table.order()),
                __etag: -1
            }
            
            dataService.setDocument(collection, tableStateData, { scopeType: "User" }).then(function(doc) {                
            });
        });      
    }

    function findSearchQueryThen(projectId, planId, funcNext) {
        VSS.require(
            [ "VSS/Service", "TFS/WorkItemTracking/RestClient" ],
            function(VSS_Service,                
                     Tfs_WorkItemTracking_RestClient) {

                tfsWorkItemTrackingService = VSS_Service.getCollectionClient(Tfs_WorkItemTracking_RestClient.WorkItemTrackingHttpClient);

                tfsWorkItemTrackingService
                    .searchQueries(projectId, planId)
                    .then(
                        function(queries) {
                            if(queries.value.length > 0) {

                                funcNext(queries.value[0]);
                            }
                            else {
                                funcNext(null);
                            }
                        }
                    );                    
            });
    }

    function removeFromAllProjectsAndAllPlans(allProjects, allPlans, projectId, planId) {           
        
        if(allProjects.has(projectId) === true) {

            // allProjects.delete(projectId);
            let project = allProjects.get(projectId);
            let index = project.plans.findIndex(function(p) { return p.id === planId; });

            project.plans.splice(index, 1); 
        }  

        if(allPlans.has(planId) === true) {            
            allPlans.delete(planId);
        }        
    }

    function addToAllProjectsAndAllPlans(allProjects, allPlans, project, plan) {       

        // retain link from plan to project
        let haveValidPlan = plan !== undefined && plan !== null;

        if(haveValidPlan === true) {
            plan['project'] = project;
        }

        // add plan to project "plans" collection
        if(project.plans !== undefined && project.plans !== null) {

            if(haveValidPlan === true) {
                project.plans.push(plan)
            }
        }
        else {
            project['plans'] = haveValidPlan === true ? [ plan ] : [];
        }

        // retain project in allProjects
        if(allProjects.has(project.id) === false) {
            allProjects.set(project.id, project);
        }

        // retain plan in allPlans
        if(haveValidPlan === true && allPlans.has(plan.id) === false) {            
            allPlans.set(plan.id, plan);
        }
    }

    function createOrUpdateRelease(self, release) {
        /*
            projectId:  projectId,
            planId:     planId,
            name:       name    .val(),
            version:    version .val(),
            area:       area    .val(),
            squad:      squad   .val(),
            testEnv:    testEnv .val(),
            po:         po      .val(),
            date:       date    .val(),
            status:     status  .val()
        */
            let tfsCoreService                = null;
            let tfsWorkItemService            = null;
            let tfsWorkItemTrackingService    = null;
            // required api services
            let required = [
                "VSS/Service",
                "TFS/Core/RestClient",
                "TFS/Work/RestClient",
                "TFS/WorkItemTracking/RestClient",
            ];

            let updateSecurityNamespaceIdWithNewSubjectDescriptors = function(authHeader, release, namespaceId, subjectDescriptors) {
                new Promise(
                    function(onSuccess, onError) {
                        // POST https://dev.azure.com/{organization}/_apis/accesscontrolentries/{securityNamespaceId}?api-version=6.0
                        let url = 'https://dev.azure.com/' + VSS.getWebContext().account.name + '/_apis/accesscontrolentries/' + namespaceId
                                + '?api-version=6.0';
    
                        subjectDescriptors
                            .forEach(
                                function(subjectDescriptor) { 
    
                                    security.getDescriptorFromSubjectDescriptor(authHeader, subjectDescriptor)
                                        .then(function(descriptor) {
    
                                            /**
                                                 {
                                                     "token": "Plan/33f8b29b-cced-48d2-8dcd-c39cfab92ffd/881e6b62-e3ff-4064-9d0c-cdd4a1799709",
                                                    "merge": true,
                                                    "accessControlEntries": [
                                                        {
                                                        "descriptor": "Microsoft.TeamFoundation.Identity;S-1-9-1551374245-1788015854-645789251-3183677911-4115539234-1-1111702339-236582976-3072162915-2739947521",
                                                        "allow": 8,
                                                        "deny": 0,
                                                        "extendedInfo": {
                                                            "effectiveAllow": 8,
                                                            "effectiveDeny": 0,
                                                            "inheritedAllow": 8,
                                                            "inheritedDeny": 0
                                                        }
                                                        }
                                                    ]
                                                }
                                            */
    
                                            let data = {
                                                token: 'Plan/' + VSS.getWebContext().project.id + '/' + release.planId,
                                                merge: true,
                                                accessControlEntries: [{
                                                    descriptor: descriptor,
                                                    allow: 15,
                                                    deny: 0,
                                                    extendedInfo: {
                                                        effectiveAllow: 15,
                                                        effectiveDeny: 0,
                                                        inheritedAllow: 15,
                                                        inheritedDeny: 0
                                                    }
                                                }]
                                            }
        
                                            $.ajax({
                                                type: "POST",
                                                url: url,
                                                beforeSend: function (xhr){ 
                                                    xhr.setRequestHeader('Authorization', authHeader);
                                                },
                                                data: JSON.stringify(data),
                                                contentType: 'application/json',
                                                dataType: 'json',
                                                success: function (result, status, xhr) {   
                                                    onSuccess(result);
                                                },
                                                error: onError
                                            });
                                        }
                                    );
                                }
                        );
                    }
                );
            }            

            let updateReleaseSecurity = function(release) {
                // set additional security
                appConfig
                    .loadConfigData().then(
                        function(config) {
                            let subjectDescriptors = config !== null && config.additionalEditors !== undefined && config.additionalEditors !== null 
                                ? config.additionalEditors.split(',')
                                : [];

                            if(subjectDescriptors.length > 0) {

                                let data = {};

                                security
                                    .getBearerToken()
                                    .then((authHeader) => {
                                        data['authHeader'] = authHeader; 
                                        return security.getSecurityNamespaceIdForPlans(data.authHeader); 
                                    })
                                    .then((namespaceId) => { 
                                        data['namespaceId'] = namespaceId;  
                                        return security.getSubjectDescriptorsForProductOwners(data.authHeader, release.productOwner.split(',').map((s) => s.trim()));
                                    })
                                    .then((poSubjectDescriptors) => { 
                                        subjectDescriptors = subjectDescriptors.concat(poSubjectDescriptors); 
                                        return security.getSubjectDescriptorsForSquads(data.authHeader, release.squad.split(',').map((s) => s.trim()));
                                    })
                                    .then((squadSubjectDescriptors) => { 
                                        subjectDescriptors = subjectDescriptors.concat(squadSubjectDescriptors);  
                                        return updateSecurityNamespaceIdWithNewSubjectDescriptors(data.authHeader, release, data.namespaceId, subjectDescriptors);
                                    });
                            }
                        }
                    ); 
            }

            let createNewPlan = function(release) {
                
                tfsCoreService
                    .getTeams(release.projectId)
                    .then(function(teams) {

                        // debugger;

                        let properties = JSON.parse(`
                        {
                            "teamBacklogMappings": [{
                                "teamId": "0e8c241d-b012-4572-8034-a873060f4378",
                                "categoryReferenceName": "Microsoft.FeatureCategory"
                            }],
                            "cardSettings": {
                                "fields": {
                                    "showId": false,
                                    "showAssignedTo": true,
                                    "assignedToDisplayFormat": "avatarAndFullName",
                                    "showState": true,
                                    "showTags": true,
                                    "showParent": false,
                                    "showEmptyFields": false,
                                    "showChildRollup": false,
                                    "additionalFields": null,
                                    "coreFields": [{
                                        "referenceName": "System.AssignedTo",
                                        "displayName": "Assigned To",
                                        "fieldType": "string",
                                        "isIdentity": true
                                    }, {
                                        "referenceName": "System.State",
                                        "displayName": "State",
                                        "fieldType": "string",
                                        "isIdentity": false
                                    }, {
                                        "referenceName": "System.Tags",
                                        "displayName": "Tags",
                                        "fieldType": "plainText",
                                        "isIdentity": false
                                    }]
                                }
                            },
                            "styleSettings": [],
                            "tagStyleSettings": []
                        }`);

                        let matchedTeams = teams.filter(function(team) { return team.name === release.squad});

                        if(matchedTeams.length > 0) {
                            properties.teamBacklogMappings[0].teamId = matchedTeams[0].id;
                        }

                        let newPlan = {
                            description:    release.planDescription,
                            name:           release.planName,
                            properties:     properties,
                            type:           'deliveryTimelineView'
                        }
        
                        tfsWorkItemService
                            .createPlan(newPlan, release.projectId)
                            .then(function(p) {
                                
                                // plan added, update the table...

                                // update release with newly minted plan id
                                release.planId = p.id;

                                self._releases.push(release);
                      
                                // clear and add updated data
                                self._table.clear().draw(false);
                                self._table.rows.add(filterReleasesByStatus(self._releases, $('#releases_search_and_select select').val()));
                                self._table.draw(false);   
                                
                                // retain plan and project in "allPlans" & "allProjects" collections
                                {
                                    let project = self._configuration.allProjects.get(release.projectId);

                                    addToAllProjectsAndAllPlans(self._configuration.allProjects, 
                                                                self._configuration.allPlans, 
                                                                project,
                                                                p);
                                }

                                // create a query (if needed)
                                self.createReleaseQueryWithRequiredFieldCriteria(p.id);

                                // save the release to remote storage
                                release.saveToStorage();

                                // make sure plan has all the required security applied
                                updateReleaseSecurity(release);
                            });    
                    }
                );
            }

            let updatePlan = function(release) {

                // get the plan to update
                tfsWorkItemService
                    .getPlan(release.projectId, release.planId)
                    .then(
                        function(thePlan) {

                            let updatedPlan = {
                                description:    release.planDescription,
                                name:           release.planName,
                                
                                // these don't change...
                                properties:     thePlan.properties,
                                revision:       thePlan.revision,
                                type:           thePlan.type
                            }

                            tfsWorkItemService
                                .updatePlan(updatedPlan, release.projectId, release.planId)
                                .then(function(p) {

                                    let data = self._releases.find(function(r) { return r.projectId == release.projectId && r.planId == release.planId});

                                    if(data !== null) {
                                        data.name           = release.name;
                                        data.version        = release.version;
                                        data.area           = release.area;
                                        data.squad          = release.squad;
                                        data.productOwner   = release.productOwner;
                                        data.testEnv        = release.testEnv;
                                        data.status         = release.status;
                                        data.date           = release.date;
                                        data.description    = release.description;
                                    }  

                                    // clear and add updated data
                                    self._table.clear().draw(false);
                                    self._table.rows.add(filterReleasesByStatus(self._releases, $('#releases_search_and_select select').val()));
                                    self._table.draw(false);

                                    // create a query (if needed)
                                    self.createReleaseQueryWithRequiredFieldCriteria(p.id);  

                                    // save the release to remote storage
                                    release.saveToStorage();

                                    // make sure plan has all the required security applied
                                    updateReleaseSecurity(release);
                                });                             
                        }
                    );
            }

            VSS.require(
                    required,
                    function(VSS_Service,
                             Tfs_Core_RestClient,
                             Tfs_Work_RestClient,
                             Tfs_WorkItemTracking_RestClient) {

                        // retain the Core and WorkItem services
                        tfsCoreService                = VSS_Service.getCollectionClient(Tfs_Core_RestClient.CoreHttpClient);
                        tfsWorkItemService            = VSS_Service.getCollectionClient(Tfs_Work_RestClient.WorkHttpClient);
                        tfsWorkItemTrackingService    = VSS_Service.getCollectionClient(Tfs_WorkItemTracking_RestClient.WorkItemTrackingHttpClient);

                        let obj = releases.createNewRelease();

                        Object.assign(obj, release);

                        if(obj.planId === undefined || obj.planId === null || obj.planId === '0' || obj.planId === 0) {

                            // create a new plan                            
                            createNewPlan(obj);
                        }
                        else {
                
                            // udate an existing plan
                            updatePlan(obj);
                        }
                    });
    }

    function showReleaseDialog(self, title, projectId, planId) {

        VSS
            .getService(VSS.ServiceIds.Dialog)
            .then(
                function(dialogService) {

                    let newReleaseForm;
                    let extensionCtx = VSS.getExtensionContext();
                    // Build absolute contribution ID for dialogContent
                    let formContributionId = "releases-overview-hub-new-release-contribution";
                    let formContributionFullId = extensionCtx.publisherId + "." + extensionCtx.extensionId + "." + formContributionId;
                    let targetPlanId = planId !== undefined && planId !== null ? planId : '0';

                    // Show dialog
                    let dialogOptions = {
                        title: title,
                        width: 800,
                        height: 710,
                        urlReplacementObject: { 
                            projectId: projectId,
                            planId: targetPlanId,
                        },
                        getDialogResult: function() {
                            // Get the result from registrationForm object
                            return newReleaseForm ? newReleaseForm.getFormData() : null;
                        },
                        okCallback: function (release) {

                            // Log the result to the console
                            console.log(JSON.stringify(release));

                            /*
                                projectId:  projectId,
                                planId:     planId,
                                name:       name    .val(),
                                version:    version .val(),
                                area:       area    .val(),
                                squad:      squad   .val(),
                                testEnv:    testEnv .val(),
                                po:         po      .val(),
                                date:       date    .val(),
                                status:     status  .val()
                            */

                            if( true
                                && release.projectId === projectId
                                && release.planId === targetPlanId) {

                                createOrUpdateRelease(self, release);
                            }
                        }                    
                    };

                    dialogService
                        .openDialog(formContributionFullId, dialogOptions)
                        .then(
                            function(dialog) {

                                dialog
                                    .getContributionInstance(formContributionId)
                                    .then(
                                        function (form) {
                                
                                            // Keep a reference of registration form instance (to be used previously in dialog options)
                                            newReleaseForm = form;
                                            
                                            // Subscribe to form input changes and update the Ok enabled state
                                            newReleaseForm
                                                .attachFormChanged(
                                                    function(isValid) {
                                                        dialog.updateOkButton(isValid);
                                                    }
                                                );
                                            
                                            // Set the initial ok enabled state
                                            newReleaseForm
                                                .isFormValid()
                                                .then(
                                                    function (isValid) {
                                                        dialog.updateOkButton(isValid);
                                                    }
                                                );
                                        }
                                    );                            
                            }
                        );
                }
            );
    }

    function showConfirmReleaseDeleteDialog(self, projectId, planId) {

        VSS
            .getService(VSS.ServiceIds.Dialog)
            .then(
                function(dialogService) {

                    let deleteReleaseForm;
                    let extensionCtx = VSS.getExtensionContext();
                    // Build absolute contribution ID for dialogContent
                    let formContributionId = "releases-overview-hub-delete-release-confirm-contribution";
                    let formContributionFullId = extensionCtx.publisherId + "." + extensionCtx.extensionId + "." + formContributionId;
                    let targetPlanId = planId !== undefined && planId !== null ? planId : '0';

                    // Show dialog
                    let dialogOptions = {
                        title: 'Delete Release',
                        width: 650,
                        height: 220,
                        okText: "Delete",
                        urlReplacementObject: { 
                            projectId: projectId,
                            planId: targetPlanId,
                        },
                        getDialogResult: function() {
                            // Get the result from registrationForm object
                            return deleteReleaseForm ? deleteReleaseForm.getFormData() : null;
                        },
                        okCallback: function (data) {

                            if( true
                                && data.projectId === projectId
                                && data.planId === targetPlanId) {

                                    let release = releases.createReleaseFromPlan(projectId, self._configuration.allPlans.get(planId));                   
                                    let isConfirmed = data.action === 'DELETE' && data.name === release.name;
                            
                                    if(isConfirmed === true) {
                            
                                        console.log('deletion of release ' + release.name + ' CONFIRMED (planid:' + planId + ')');

                                        self
                                            ._tfsWorkItemService
                                            .deletePlan(projectId, planId)
                                            .then(
                                                function() {
                            
                                                    console.log('deleted plan ' + planId);

                                                    // remove from "all projects" & "all plans"
                                                    removeFromAllProjectsAndAllPlans(self._configuration.allProjects, 
                                                                                     self._configuration.allPlans, 
                                                                                     projectId, 
                                                                                     planId);
                            
                                                    // remove from table
                                                    {
                                                        let index = self._releases.findIndex(function(r) { return r.planId === planId; });
                                                        self._releases.splice(index, 1);
                                        
                                                        // clear and add updated data
                                                        self._table.clear().draw(false);
                                                        self._table.rows.add(filterReleasesByStatus(self._releases, $('#releases_search_and_select select').val()));
                                                        self._table.draw(false);   
                                                    }
                            
                                                    // remove the associated query (if it exists)
                                                    self
                                                        ._tfsWorkItemTrackingService
                                                        .searchQueries(projectId, planId)
                                                        .then(
                                                            function(queries) {
                                        
                                                                if(queries.value.length == 1) {
                                                                    console.log('deleted releases associated query ' + queries.value[0].name);

                                                                    self._tfsWorkItemTrackingService.deleteQuery(projectId, queries.value[0].id)
                                                                }
                                                            }
                                                        );

                                                    // remove from storage (if it exists)
                                                    releases.deleteReleaseFromStorage(projectId, planId);
                                                }
                                            );
                                    }
                            }
                        }                    
                    };

                    dialogService
                        .openDialog(formContributionFullId, dialogOptions)
                        .then(
                            function(dialog) {

                                dialog
                                    .getContributionInstance(formContributionId)
                                    .then(
                                        function (form) {
                                
                                            // Keep a reference of registration form instance (to be used previously in dialog options)
                                            deleteReleaseForm = form;
                                            
                                            // Subscribe to form input changes and update the Ok enabled state
                                            deleteReleaseForm
                                                .attachFormChanged(
                                                    function(isValid) {
                                                        dialog.updateOkButton(isValid);
                                                    }
                                                );
                                            
                                            // Set the initial ok enabled state
                                            deleteReleaseForm
                                                .isFormValid()
                                                .then(
                                                    function (isValid) {
                                                        dialog.updateOkButton(isValid);
                                                    }
                                                );
                                        }
                                    );                            
                            }
                        );
                }
            );
    }

    function filterReleasesByStatus (releases, status) {

        return status === 'released' || status === 'scheduled'
                ? releases.filter(function(r) { return r.status === status })
                : releases;
    }   

    function filterReleasesById (releases, releaseId) {
        return releases.filter(function(r) { return r.planId === releaseId })
    }

    // private function to build and initialise the table content
    function initReleaseTable (self) {

        let containerId = self._configuration.containerId;
        let projects = self._configuration.allProjects;

        if( true
            && containerId !== undefined
            && containerId !== null
            && projects !== undefined
            && projects !== null) {

            //let projNameExpr = new RegExp(self._configuration.projectNameFilterExpr);
            //let planNameExpr = new RegExp(self._configuration.planNameFilterExpr);
            //let planDescExpr = new RegExp(self._configuration.planDescFilterExpr);
            
            let projNameExpr = new RegExp('.*');
            let planNameExpr = new RegExp('.*');
            let planDescExpr = new RegExp('.*');

            let selectableProjectPlans = new Map();
            let selectablePlans = new Map();

            let fillSelectablePlansWithThoseFilteredIn = function(project, plan) {

                if( true
                    && planNameExpr.test(plan.name) === true
                    && planDescExpr.test(plan.description) === true) {

                    // map of ALL selecteable plans
                    selectablePlans.set(plan.id, plan);

                    // fill map of project -> plan(s)
                    if(selectableProjectPlans.has(project.id) === false) {
                        // new map entry with array of this plan
                        selectableProjectPlans.set(project.id, { project: project, plans: [ plan ] });                                                
                    }
                    else {

                        // project map entry already exists, push the plan
                        selectableProjectPlans.get(project.id).plans.push(plan);
                    }
                }
                else {

                    if(planNameExpr.test(plan.name) === false) {
                        console.log('Skipping plan with name \"' + plan.name + '\" because it does NOT match the expression \"' + self._configuration.planNameFilterExpr + '\"');
                    }

                    if(planDescExpr.test(plan.description) === false) {
                        console.log('Skipping plan (' + plan.name + ') with description \"' + plan.description + '\" because it does NOT match the expression \"' + self._configuration.planDescFilterExpr + '\"');
                    }
                }                
            }

            // build map of valid project -> plan(s)
            // build map of valid plan(s)
            projects
                .forEach(
                    function(project) {

                        console.log('Trying to load plans for project ' + project.name + '...');

                        if(projNameExpr.test(project.name) === true) {

                            if( true
                                && project.plans !== undefined
                                && project.plans !== null
                                && project.plans.length > 0) {

                                // each plan is an option...
                                project.plans.forEach(
                                    function(plan) {

                                        fillSelectablePlansWithThoseFilteredIn(project, plan);
                                    }
                                )
                            }
                        }
                        else {
                            console.log('Skipping project with name \"' + project.name + '\" because it does NOT match the expression \"' + self._configuration.projectNameFilterExpr + '\"');
                        }
                    }
                );

            let fillTableWithSelectablePlans = function() {

                let releasesContainer = $(containerId);
                let releasesTablebody = $(containerId + ' table tbody');
                
                 // fill drop down with plans...
                if(selectableProjectPlans.size > 0) {

                    // fill the drop down
                    selectableProjectPlans.forEach(
                        function(projectPlans) {

                            let project = projectPlans.project;
                            let plans = projectPlans.plans;

                            plans.forEach(
                                function(plan) {
                                    
                                    let release = releases.createReleaseFromPlan(project.id, plan);
                                    
                                    self._releases.push(release);
                                }
                            );
                        }
                    )                      
                }

                // turn table into datatable and retain it                
                self._table = $(containerId + ' table').DataTable({
                    lengthMenu: [ 10, 15, 20, 25 ],
                    scrollY: "60vh",
                    scrollCollapse: true,                    
                    data: self._releases,
                    columns: [
                        { 
                            data: 'planId', 
                            visible: false
                        },
                        {
                            data: null,
                            render: function ( data, type, row ) {
                                return '<a onclick="app.showPopupMenu(\'' + row.projectId + '\',\'' + row.planId + '\');return false;">' + row.name + '</a>';
                            }                            
                        },
                        { data: 'version'           },
                        { data: 'area'              },
                        { data: 'squad'             },
                        { data: 'productOwner'      },
                        { data: 'testEnv'           },
                        { data: 'status'            },
                        { data: 'date'              },
                        { data: 'shortDescription'  }
                    ]            
                });
           
                // add drop down so user can select an "auto" filter
                {
                    $('#releases_table_wrapper').prepend('<div id=\'releases_search_and_select\' style=\'float:right\'></div>');
                    $('#releases_search_and_select').append($('#releases_table_filter'));
                    $('#releases_table_filter').css('float', 'right');

                    // new span
                    let span = $('<span>');
                    {
                        // new label
                        let label = $('<label style=\'margin-right:10px; color:rgba(0, 0, 0, 0.55);\' class=\'dataTables_filter\'>').text('Status filter: ');

                        // new select as child of label
                        {
                            let select = $('<select>');

                            select.append($('<option value=\'all\'>').text('all'));
                            select.append($('<option value=\'scheduled\'>').text('scheduled'));
                            select.append($('<option value=\'released\'>').text('released'));

                            select.change(function() {

                                // clear and add updated data
                                self._table.clear().draw(false);
                                self._table.rows.add(filterReleasesByStatus(self._releases, this.value));
                                self._table.draw(false);

                                saveTableState(self);
                            });

                            label.append(select);
                        }

                        span.append(label);
                    }

                    $('#releases_search_and_select').append(span);
                }

                // restore table state, then configure table event handling
                restoreTableStateThen(self, function() {
                    // capture when search filter is applied
                    self._table.on( 'search.dt', function () {
                        saveTableState(self);
                    });

                    // capture when page length is changed
                    self._table.on( 'length.dt', function ( e, settings, len ) {
                        saveTableState(self);
                    });  

                    // capture when column ordering changes
                    self._table.on( 'order.dt', function () {
                        saveTableState(self);
                    });
                });

                // Add event listener for opening and closing details
                $(containerId + ' table tbody').on('dblclick', 'tr', function () {

                    //var data = table.row( this ).data();
                    //alert( 'You clicked on '+data[0]+'\'s row' );
                }); 
                
                releasesContainer.show();  
            }

            fillTableWithSelectablePlans();
        }
    }      

    return {

        _isInitialising: false,
        _isInitialised: false,

        /* Configuration data

            {
                allProjects:            projects,
                allPlans:               plans,
                containerId:            containerId,
                querySubFolder:         'Releases',
                queryFieldName:         'Fix Version'
            }
        */
        _configuration: null,

        /* Services */
        _tfsCoreService: null,
        _tfsWorkItemService: null,
        _tfsWorkItemTrackingService: null,
        _table: null,
        _releases: [],

        _queryFieldName: null,
        _querySubFolder: null,


        init: function(containerId, onInitialised) {

            let self = this;

            if(self._isInitialising === true || self._isInitialised === true) {

                // nothing to do!
                return;
            }

            // make as being in the processes of initialising
            self._isInitialising = true;

            let initialiseAppWithProjects = function(projects) {

                let allProjects = new Map();
                let allPlans = new Map();

                // for each project...
                projects.forEach(
                    function(project) {

                        // get plan for the project
                        self._tfsWorkItemService
                            .getPlans(project.id)
                            .then(
                                function(plans) {

                                    // retain each plan
                                    if(plans.length > 0) {
                                        plans
                                            .forEach(
                                                function(plan) { 

                                                    addToAllProjectsAndAllPlans(allProjects, allPlans, project, plan);
                                                }
                                            );
                                    }
                                    else {
                                        addToAllProjectsAndAllPlans(allProjects, allPlans, project);
                                    }
                                }
                            )
                            .then(
                                function() {
                                    // Have the plans for ALL the projects have been retrieved?
                                    if(self._configuration === null && allProjects.size === projects.length) {

                                        // looks like all the plans for all the projects have been retreived, time to finish initialising the app...

                                        // set self._configuration to something so it's NOT null so that this code does NOT get called again...
                                        self._configuration = {}

                                        appConfig
                                            .loadConfigData().then(
                                                function(config) {
                                
                                                    // setup self._configuration again with real data
                                                    self._configuration = {
                                                        allProjects:            allProjects,
                                                        allPlans:               allPlans,
                                                        containerId:            containerId,
                                                        queryFieldName:         config !== null && config.queryFieldName   !== undefined && config.queryFieldName   !== null ? config.queryFieldName   : 'Fix Version',
                                                        querySubFolder:         config !== null && config.querySubFolder   !== undefined && config.querySubFolder   !== null ? config.querySubFolder   : 'Releases'
                                                    }

                                                    // build the release table
                                                    initReleaseTable(self);

                                                    // make app as having been initialised
                                                    self._isInitialised = true;
                                                    self._isInitialising = false;

                                                    // call onInitialised
                                                    if(onInitialised !== undefined && onInitialised !== null) {
                                                        onInitialised(allProjects);
                                                    }
                                                }
                                            );
                                    }
                                }
                            );
                    }
                );                
            }

            let getAllProjectsAndPlans = function() {
               
                /*
                // get all projects...
                self._tfsCoreService
                    .getProjects()
                    .then(
                        // process the collection of projects
                        function(projects) {
                            
                            // ONLY the current project thanks
                            initialiseAppWithProjects(projects);
                        }
                    );
                    */
                self._tfsCoreService
                    .getProject(VSS.getWebContext().project.id)
                    .then(
                        function(project) {
                            
                            // ONLY the current project thanks
                            initialiseAppWithProjects([ project ]);
                        }
                    );
            }

            // required api services
            let required = [
                "VSS/Service",
                "TFS/Core/RestClient",
                "TFS/Work/RestClient",
                "TFS/WorkItemTracking/RestClient",
                "TFS/Dashboards/WidgetHelpers"
            ];

            VSS.require(
                    required,
                    function(VSS_Service,
                             Tfs_Core_RestClient,
                             Tfs_Work_RestClient,
                             Tfs_WorkItemTracking_RestClient,
                             Tfs_WidgetHelpers) {

                        // retain the Core and WorkItem services
                        self._tfsCoreService                = VSS_Service.getCollectionClient(Tfs_Core_RestClient.CoreHttpClient);
                        self._tfsWorkItemService            = VSS_Service.getCollectionClient(Tfs_Work_RestClient.WorkHttpClient);
                        self._tfsWorkItemTrackingService    = VSS_Service.getCollectionClient(Tfs_WorkItemTracking_RestClient.WorkItemTrackingHttpClient);

                        Tfs_WidgetHelpers.IncludeWidgetStyles();

                        getAllProjectsAndPlans();
                    });
        },

        redirectToReleaseQuery: function(projectId, planId) {
            VSS.require(
                [ "VSS/Service", "TFS/Core/RestClient", "TFS/WorkItemTracking/RestClient" ],
                function(VSS_Service,
                         Tfs_Core_RestClient,
                         Tfs_WorkItemTracking_RestClient) {

                    tfsCoreService                = VSS_Service.getCollectionClient(Tfs_Core_RestClient.CoreHttpClient);
                    tfsWorkItemTrackingService    = VSS_Service.getCollectionClient(Tfs_WorkItemTracking_RestClient.WorkItemTrackingHttpClient);

                    tfsWorkItemTrackingService
                        .searchQueries(projectId, planId)
                        .then(
                            function(queries) {
                                if(queries.value.length > 0) {
                                    tfsCoreService
                                        .getProject(projectId)
                                        .then(
                                            function(project) {
                                                // https://vendorpanel.visualstudio.com/VendorPanel%20Website/_queries/query/0ba289ec-0ac8-428b-b23c-173d62207642/
                                                let url = project._links.web.href + '/_queries/query/' + queries.value[0].id;
                                                
                                                window.open(url, '_blank').focus();
                                            }
                                        );
                                }
                            }
                        );                    
                });
        },

        redirectToReleasePlan: function(projectId, planId) {
   
            VSS.require(
                [ "VSS/Service", "TFS/Core/RestClient" ],
                function(VSS_Service,
                         Tfs_Core_RestClient) {

                    tfsCoreService = VSS_Service.getCollectionClient(Tfs_Core_RestClient.CoreHttpClient);

                    tfsCoreService
                        .getProject(projectId)
                        .then(
                            function(project) {
                                // https://vendorpanel.visualstudio.com/VendorPanel%20Website/_deliveryplans/plan/3dc8cb5d-a8e1-45ef-a17b-a21d7f4f3d9f
                                let url = project._links.web.href + '/_deliveryplans/plan/' + planId;

                                window.open(url, '_blank').focus();
                            }
                        );                 
                });
        },                

        createReleaseQueryWithRequiredFieldCriteria: function(planId) {

            let self = this;
    
            return new Promise(
                function(onSuccess, onError) {

                    if(planId === undefined || planId === null || planId === '') {
        
                        onError('invalid plan id');
                        return;
                    }
                        
                    if(self._configuration.allPlans.has(planId) === true) {
            
                        let plan = self._configuration.allPlans.get(planId);
                        let theProject = plan.project;
                        
                        self
                            ._tfsWorkItemTrackingService
                            .searchQueries(theProject.id, planId)
                            .then(
                                function(queries) {
            
                                    if(queries.value.length <= 0) {
            
                                        let createReleaseQuery = function(parentFolder, prefix) {
            
                                            let wiql = 'SELECT [System.Id],[System.WorkItemType],[System.Title],[System.AssignedTo],[System.State],[System.Tags],[System.AreaPath],[System.IterationPath] FROM workitemLinks WHERE ([Source].[' + self._configuration.queryFieldName + '] CONTAINS \'' + plan.id + '\') AND ([System.Links.LinkType] = \'System.LinkTypes.Hierarchy-Forward\') AND ([Target].[System.WorkItemType] <> \'\') MODE (Recursive)';
                                            let namePrefix = prefix === undefined || prefix === null ? '' : prefix;
                                            
                                            // drop the version number
                                            let planNameNoVersion = plan.name.replace(/\d+(.\d+)*(\s*-\s*)?/, "").trim();
                                            let queryName = namePrefix + planNameNoVersion + ' (' + plan.id + ')';                                
                                            let newQuery = {
                                                name: queryName,
                                                wiql: wiql
                                            }
            
                                            self
                                                ._tfsWorkItemTrackingService
                                                .createQuery(newQuery, theProject.id, parentFolder.path)
                                                .then(function(p) {
                                                    // query added
                                                    console.log('Created query ' + queryName);
        
                                                    // all done!
                                                    onSuccess();
                                                },
                                                onError);
                                        }
            
                                        let createReleaseFolderThenQuery = function(parentFolder, subFolderName) {
            
                                            let newQuery = {
                                                name: subFolderName,
                                                isFolder: true
                                            }
                                            self
                                                ._tfsWorkItemTrackingService
                                                .createQuery(newQuery, theProject.id, parentFolder.path)
                                                .then(function(subFolder) {
            
                                                    // query folder added, now add the query
                                                    createReleaseQuery(subFolder);
                                                },
                                                onError);
                                        }
            
                                        // release query does NOT exist, create it
            
                                        self
                                            ._tfsWorkItemTrackingService
                                            .getQueries(theProject.id, 'none', '1')
                                            .then(
                                                function(queries) {
            
                                                    let subFolderName = self._configuration.querySubFolder;
            
                                                    // get the public shared queries folder
                                                    let sharedQueriesFolder = queries.filter(function(query) { return query.isFolder === true && query.isPublic === true && query.name.toUpperCase() === 'SHARED QUERIES'; });
            
                                                    // try to get the subfolder name Releases
                                                    let releasesFolder = (sharedQueriesFolder.length === 1 && sharedQueriesFolder[0].hasChildren === true && subFolderName !== null)
                                                                            ? sharedQueriesFolder[0].children.filter(function(child) { return child.name.toUpperCase() === subFolderName.toUpperCase() && child.isFolder === true })
                                                                            : [];
            
                                                    // try to get the query name Releases
                                                    let releasesQuery = (sharedQueriesFolder.length === 1 && sharedQueriesFolder[0].hasChildren === true && subFolderName !== null)
                                                                            ? sharedQueriesFolder[0].children.filter(function(child) { return child.name.toUpperCase() === subFolderName.toUpperCase() && child.isFolder === false })
                                                                            : [];                                                                
            
                                                    if(releasesFolder.length > 0) {
            
                                                        // Got a subfolder name 'Releases" create a new query in the Releases sub folder
                                                        createReleaseQuery(releasesFolder[0]);
                                                    }
                                                    else if(releasesQuery.length > 0) {
            
                                                        // Got query named 'Releases' in the 'Shared Queries" folder.  Put new query in the root folder
                                                        createReleaseQuery(sharedQueriesFolder[0], 'Release ');
                                                    }
                                                    else {
            
                                                        if(subFolderName !== null) {
                                                            // releases folder does NOT exist, create it and then create the new query in it
                                                            createReleaseFolderThenQuery(sharedQueriesFolder[0], subFolderName);
                                                        }
                                                        else {
            
                                                            // No sub folder configured.  Put new query in the root folder
                                                            createReleaseQuery(sharedQueriesFolder[0], 'Release ');
                                                        }
                                                    }
                                                },
                                                onError
                                            );
                                    } 
                                    else {
                                        onSuccess();
                                    }
                                }
                            );
                    }
                }
            );
        },

        deleteRelease: function(projectId, planId) {
            let self = this;
    
            return new Promise(
                function(onSuccess, onError) {

                    if(false
                        || projectId === undefined 
                        || projectId === null 
                        || projectId === '' 
                        || planId === undefined 
                        || planId === null 
                        || planId === '') {
        
                        onError('invalid project and/or plan id');
                        return;
                    }
                        
                    if(    true
                        && self._configuration.allProjects.has(projectId) === true
                        && self._configuration.allPlans.has(planId) === true) {

                        showConfirmReleaseDeleteDialog(self, projectId, planId);
                    }
                }
            );
        },

        showNewReleaseDialog: function() {
                  
            showReleaseDialog(this, 'Create a new release', VSS.getWebContext().project.id);
        },

        showReleaseDialog: function(projectId, planId) {

            showReleaseDialog(this,'Release details', projectId, planId);
        },  
        
        showPopupMenu: function(projectId, planId) {

            let self = this;
            let popupX = event.clientX;
            let popupY = event.clientY;

            findSearchQueryThen(projectId, planId, function(query) {

                // https://github.com/jlswcx/popmenu
                let items = query !== null
                                ? {
                                    details : {
                                        name : 'Show details'
                                    },
                                    query : {
                                        name : 'Show all work items'
                                    },
                                    plan : {
                                        name: 'Go to plan',
                                        divid : true
                                    },
                                    delete : {
                                        name : 'Delete release (and associated query)'
                                    }
                                }
                                : {
                                    details : {
                                        name : 'Show details'
                                    },
                                    createQuery : {
                                        name : 'Create a query to show all work items'
                                    },                                    
                                    plan : {
                                        name: 'Go to plan',
                                        divid: true
                                    },
                                    delete : {
                                        name : 'Delete release'
                                    }                                
                                };

                popmenu({
                    x: popupX,
                    y: popupY,
                    items : items,
                    callback: function(item) {

                        let id = $(item).attr('id');

                        if(id === "details") {
                            self.showReleaseDialog(projectId, planId);
                        }
                        else if(id === "query") {
                            self.redirectToReleaseQuery(projectId, planId);
                        }
                        else if(id === "createQuery") {
                            self
                                .createReleaseQueryWithRequiredFieldCriteria(planId)
                                .then(
                                    function() { 
                                        self.redirectToReleaseQuery(projectId, planId); 
                                    });
                        }
                        else if(id === "plan") {
                            self.redirectToReleasePlan(projectId, planId);
                        }
                        else if(id === "delete") {
                            self.deleteRelease(projectId, planId)
                        }                 
                        else {
                            self.showReleaseDialog(projectId, planId);
                        }                  
                    }	    		
                });
            });
        }        
    }
})();